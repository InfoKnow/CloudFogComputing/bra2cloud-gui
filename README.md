<img align="center" src="https://gitlab.com/InfoKnow/CloudFogComputing/BRA2Cloud/raw/master/images/logo_space.png" height="200"/>

BRA2Cloud - GUI Web Application
=============

This module contains the BRA2Cloud GUI module that executes users bag-of-tasks applications using our agent-based architecture, as defined at the [parent project website](https://gitlab.com/InfoKnow/CloudFogComputing/BRA2Cloud).

Click [here](https://gitlab.com/InfoKnow/CloudFogComputing/BRA2Cloud) to visit our parent project website.

## Requirements:
- Java (8+) - A programming language that produces software for multiple platforms.
- PostgreSQL (9.6+) - A powerful, open source object-relational database system.
- Tomcat (7+) - An open source implementation of the Java Servlet for web.
- MySQL (8+) - The world's most popular open source database (optional).
- MyCBR (3.1+) - An open-source similarity-based retrieval tool.
- A Linux/OSX bash environment with Gnuplot (5+) installed.
- A set of libraries available at lib folder.

## Project Website
- http://bra2cloud.cic.unb.br

:mega: We are working on a new project website and it will be published soon. Stay connected!

## Contributing

You're welcome to contribute to this project! Any help is appreciated!

Feel free to report issues and open merge requests.

There are several aspects you can help on:
- Improving our code and sharing with us
- Testing our solutions
- Sharing and helping other users to use this framework
- You can suggest your desired features

Please contact us for questions or to get in touch.

## License
[Apache 2.0](http://www.apache.org/licenses/LICENSE-2.0.txt)